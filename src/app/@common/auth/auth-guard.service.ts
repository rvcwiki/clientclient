import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad, Route, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { SessionService } from '../service/session.service';

@Injectable()


export class AuthGuardService implements CanActivate, CanActivateChild, CanLoad {
    constructor(
        private sessionService: SessionService,
        private authServise : AuthService
    ) { }
    canLoad(route: Route): boolean {
        return this.authServise.isLogedin();
    }
    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> | Observable<boolean> {
        throw new Error("Method not implemented.");
    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        throw new Error("Method not implemented.");
    }
}