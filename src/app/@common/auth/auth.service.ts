import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'
import { Router } from '@angular/router';
import { SessionService } from '../service/session.service';
import { ActiveUserModel } from '../model/auth.model';
import { ConnectionService } from '../service/connection.service';

@Injectable()
export class AuthService {

  constructor(private http: Http, 
    private sessionService: SessionService,
     private route: Router,
     private connectionService: ConnectionService) {
  }
  
  redirectUrl = '/pages/dashboard'
  redirectUrlLogout = '/login'

  getUser(data): Promise<any> {

    return this.connectionService.post('login', data).then(response => {
      this.sessionService.setActiveUser(<ActiveUserModel>response);
      return true
    }).catch((err) => {
      throw err
    })
  }


  isLogedin(): boolean {
    const session = this.sessionService.getActiveUser()
    if (!session) {
      this.route.navigate([this.redirectUrlLogout]);
      return false;
    } else {
      return true;
    }
  }




}
