import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map'
import { reject, resolve } from 'q';
import { SessionService } from './session.service';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class ConnectionService {

  private headers: Headers
  private ipServer = 'http://localhost:3000/';
  private authorizationTxt = 'authorization'


  constructor(

    private http: Http,
    private sessionService: SessionService,
    private route: Router, ) {


    this.headers = new Headers();
    this.headers.set('Accept', 'application/json')
    this.headers.set('Content-Type', 'application/json')
  }

  private setHeader(): Headers {
    const sign = this.sessionService.getActiveUser();
    if (sign !== null) {
      this.headers.set(this.authorizationTxt, sign.token);
    }
    return this.headers;

  }

  get(path): Promise<any> {
    return this.http.get(this.ipServer + path, { headers: this.setHeader() })
      .toPromise()
      .then((response: Response) => {
        return response.json();
      }).catch(err => {
        throw this.handlerError(err)
      })
  }

  post(path, data): Promise<any> {
    return this.http.post(this.ipServer + path, data, { headers: this.setHeader() })
      .toPromise()
      .then((response: Response) => {
        return response.json();
      }).catch(err => {
        throw this.handlerError(err)
      })
  }


  handlerError(err) {
    switch (err.status) {
      case 401:
        this.sessionService.clearActiveUser();
        this.route.navigate(['/login']);
        break;
      default: break;
    }
    return err
  }
}
