import { Component, OnInit, style } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { DashboardService } from './dashboard.service'


@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent {


  projectList: jiraProject[];
  sssProgress: string;
  modalName: string
  dataForm: FormGroup;
  findData: any;


  constructor(
    private progressService: DashboardService,
    private _fb: FormBuilder,
    private serviceModal: NgbModal
  ) {

  }

  ngOnInit() {

    this.dataForm = this._fb.group({
      key: ["Name", Validators.compose([Validators.required, Validators.nullValidator])],
      value: [null, Validators.compose([Validators.required, Validators.nullValidator])]
    })

    this.progressService.getProjectList('')
      .then(response => {
        this.projectList = response
      })

    // this.progressService.getProjectList('').subscribe((response) => {
    //   this.projectList = response;
    // })
  }

  // onChange(type) {
  //   this.dataForm.controls["key"].setValue(type);
  // }

  onSearchApi(event) {

    this.progressService.getProjectList(event)
      .then((response) => {
        this.projectList = response;
      })

  }


  openDetail(content, name, event) {
    this.modalName = name
    this.serviceModal.open(content, {
      size: "lg"
    })
    this.sssProgress = event

    setTimeout(() => {
      document.getElementsByClassName('modal-dialog')[0].setAttribute('style', 'max-width: 100vw;  width :70vw; ')
    }, 100);
  }
}




interface jiraProject {
  pjID: number;
  pjName: string;
  stDone: number;
  stInProgress: number;
  stOpen: number;
  Total: number;
}



