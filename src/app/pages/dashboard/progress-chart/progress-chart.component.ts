import { Component, OnDestroy, OnInit, Input, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { Router } from '@angular/router';
import { DashboardService } from '../dashboard.service'
@Component({
  selector: 'progress-chart',
  templateUrl: './progress-chart.component.html',
  styleUrls: ['./progress-chart.component.scss'],
})

export class ProgressChartComponent implements OnDestroy {

  data: Data;
  options: any;
  themeSubscription: any;
  @Input() projectname: any;
  @Output() pjID: EventEmitter<any> = new EventEmitter<any>();


  constructor(private theme: NbThemeService, protected rout: Router) {

  }

  ngAfterViewInit() {

    let project = this.projectname.stOpen + this.projectname.stInProgress + this.projectname.stDone


    setTimeout(() => {
      if (project == 0) {
        this.noDataChart()
      } else {
        this.dataChart()
      }
    }, 100)



  }

  ngOnDestroy(): void {

    this.themeSubscription.unsubscribe();
  }



  dataChart() {

    let perOpen = this.getPercented(this.projectname.stOpen, this.projectname.Total)
    let perInpro = this.getPercented(this.projectname.stInProgress, this.projectname.Total)
    let perDone = this.getPercented(this.projectname.stDone, this.projectname.Total)



    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;

      this.data = {
        labels: ['Open ' + perOpen + '%', 'In Progress ' + perInpro + '%', 'Done ' + perDone + '%'],
        datasets: [{
          data: [perOpen, perInpro, perDone],
          backgroundColor: [colors.infoLight, colors.warningLight, colors.successLight],
        }],
      };

      this.options = {
        maintainAspectRatio: false,
        responsive: true,
        scales: {
          xAxes: [
            {
              display: false,
            },
          ],
          yAxes: [
            {
              display: false,
            },
          ],
        },
        legend: {
          labels: {
            fontColor: chartjs.textColor,
          },
        },
      };
    });
  }

  noDataChart() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;

      this.data = {
        labels: ['No Progress'],
        datasets: [{
          data: [1],
          backgroundColor: ['#969696'],
        }],
      };

      this.options = {
        maintainAspectRatio: false,
        responsive: true,
        scales: {
          xAxes: [
            {
              display: false,
            },
          ],
          yAxes: [
            {
              display: false,
            },
          ],
        },
        legend: {
          labels: {
            fontColor: chartjs.textColor,
          },
        },
      };
    });
  }


  getPercented(data, max) {

    let percent = (data / max) * 100

    return Math.round(percent);
  }


  changePage(getID, getName) {
    let data = {
      id: getID,
      name: getName
    }
    this.rout.navigate(['/pages/task-list'], { queryParams: data });

  }

  onPopup(id){
    this.pjID.emit(id)
  }
}

interface Data {
  labels: any;
  datasets: any
}
