import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'
import { ConnectionService } from '../../@common/service/connection.service';

@Injectable()
export class DashboardService {


  constructor(private http: Http, private connectionService: ConnectionService) {

  }

  getProjectList(data): Promise<any> {
    return this.connectionService.post('project', data)
      .then(response => {
        return response;
      })
      .catch(error => {
        throw error
      })
    // return this.http.post("http://localhost:3000/project", data).map((res) => res.json());
  }

  getSss(id): Promise<any> {
    return this.connectionService.get('sss/' + id)
    .then(response =>{
      return response
    })
    .catch(error =>  {
      throw error
    })
    // return this.http.get("http://localhost:3000/sss/" + id).map((res) => res.json());
  }

  getPercent(data) {
    // return this.http.post("http://localhost:3000/project", data).map((res) => res.json());
  }




}
