import { Component, OnInit, Input } from '@angular/core';
import { DashboardService } from '../dashboard.service';

@Component({
  selector: 'sss-progress',
  templateUrl: './sss-progress.component.html',
  styleUrls: ['./sss-progress.component.scss']
})
export class SssProgressComponent implements OnInit {


  @Input() sssID: string
  @Input() sssName : string
  public sssProgress: epicProgress[]

  constructor(private service: DashboardService) { }

  ngOnInit() {
    let id = this.sssID
    this.service.getSss(id).then((response) => {
      this.sssProgress = response
    })
  }


}

interface epicProgress {
  SSS: string;
  Epic_Des: string;
  Subtask: number;
  Done: number;
  Progress: number;
}
