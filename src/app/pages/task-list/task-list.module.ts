import { NgModule } from '@angular/core';
import { DxDataGridModule, DxCheckBoxModule, DxButtonModule } from 'devextreme-angular'
import { TaskListService } from './task-list.service'
import { TaskListComponent } from './task-list.component';
import { ThemeModule } from '../../@theme/theme.module';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  imports: [

    DxDataGridModule,
    DxCheckBoxModule,
    DxButtonModule,
    ThemeModule,
    HttpClientModule
  ],
  declarations: [
    TaskListComponent,
  ],
  providers: [
    TaskListService
  ]
})
export class TaskListModule { }



