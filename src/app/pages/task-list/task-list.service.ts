import { Injectable } from '@angular/core';
import { Http, ResponseContentType, RequestMethod, Headers, } from '@angular/http'
import 'rxjs/add/operator/map'
import { ConnectionService } from '../../@common/service/connection.service';
import { resolve } from 'url';
import { reject } from 'q';



@Injectable()
export class TaskListService {

  constructor(private http: Http, private connectionService: ConnectionService) { }


  // getTaskList(id) {
  //   return this.http.get("http://localhost:3000/task/" + id).map((res) => res.json());
  // }

  getTaskList(id): Promise<any> {
    return this.connectionService.get('task/' + id).then(response => {
      return response
    }).catch(error => {
      throw error
    })
  }

  getReport(paramData): Promise<any> {
    return this.connectionService.post('report', paramData)
      .then(response => {

      })
      .catch(error => {
        throw error
      })
    // getReport(data) {
    //   return this.http.post("http://localhost:3000/report/",data).map((res) => res.json());
    // }

  }


}




