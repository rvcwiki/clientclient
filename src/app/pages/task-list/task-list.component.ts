import { Component, OnInit, AfterViewInit } from '@angular/core';
import { TaskListService } from './task-list.service'
import { ActivatedRoute } from '@angular/router';
// import responsive_box from 'devextreme/ui/responsive_box';
import { saveAs } from 'file-saver/FileSaver';


@Component({
  selector: 'task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {
  public taskList: projectModel[];
  public paramData;
  public exportRes;

  constructor(private tasklistService: TaskListService, protected rout: ActivatedRoute) {

  }

  ngOnInit() {
    this.rout.queryParams.subscribe(data => {
      this.paramData = data

      this.tasklistService.getTaskList(this.paramData.id)
      .then(data => {
        this.taskList = data
      })
    })

  }

  onExport() {

      this.tasklistService.getReport(this.paramData)
      .then(res =>{

      })
      .catch(err =>{

      })
    // this.service.getReport(this.paramData)
    //   .subscribe((response) => {
    //     console.log("Response+++++" + JSON.stringify(response))
    // var blob = new Blob([response.blob()], { type: 'application/octet-stream' });
    // var blob = new Blob([response.blob()], { type: 'application/octet-stream' });
    // var filename = this.paramData.name + '.xlsx';
    // saveAs(blob, filename);
    // })

  }





  // public selectCol = {
  //   ID: true,
  //   SSS: true,
  //   Epic: true,
  //   Epic_Des: true,
  //   Task: true,
  //   Task_des: true,
  //   Subtask: true,
  //   Subtask_des: true,
  //   REPORTER: true,
  //   ASSIGNEE: true,
  //   STATUS: true
  // }

  // public showCol = {
  //   ID: true,
  //   SSS: true,
  //   Epic: true,
  //   Epic_Des: true,
  //   Task: true,
  //   Task_des: true,
  //   Subtask: true,
  //   Subtask_des: true,
  //   REPORTER: true,
  //   ASSIGNEE: true,
  //   STATUS: true
  // }


  // onSelectCol() {
  //   Object.assign(this.showCol, this.selectCol)
  // }

  // onClear() {
  //   this.selectCol.ID = false;
  //   this.selectCol.SSS = false;
  //   this.selectCol.Epic = false;
  //   this.selectCol.Epic_Des = false;
  //   this.selectCol.Task = false;
  //   this.selectCol.Task_des = false;
  //   this.selectCol.Subtask = false;
  //   this.selectCol.Subtask_des = false;
  //   this.selectCol.REPORTER = false;
  //   this.selectCol.ASSIGNEE = false;
  //   this.selectCol.STATUS = false;


  // }

}





interface projectModel {
  ID: number;
  SSS: number;
  Epic: string;
  Epic_Des: string;
  Task: string;
  Task_des: string;
  Subtask: number;
  Subtask_des: string;
  REPORTER: string;
  ASSIGNEE: string;
  STATUS: string;
}